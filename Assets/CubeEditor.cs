﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
[RequireComponent(typeof(WayPoint))]
public class CubeEditor : MonoBehaviour {
	WayPoint wayPoint;
	void Start()
	{
		wayPoint=GetComponent<WayPoint>();
			
	}
	void Update () {
		SnapToGrid();
		UpdateLabel();

	}
	void SnapToGrid()
	{
		int gridSize=wayPoint.GetGridSize();
		transform.position=new Vector3(wayPoint.GetGridPos().x*gridSize,0,wayPoint.GetGridPos().y*gridSize);
	}
	void UpdateLabel()
	{
		//int gridSize=wayPoint.GetGridSize();
		TextMesh textMesh=GetComponentInChildren<TextMesh>();
		string labelText=wayPoint.GetGridPos().x+","+wayPoint.GetGridPos().y;
		textMesh.text= labelText;
		gameObject.name=labelText;
	}
}
