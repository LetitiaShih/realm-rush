﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour {
	public WayPoint exploredFrom;
	Vector2Int gridPos;
	const int gridSize=10;
	public bool isExplored=false;
	public bool isPlaceAble=true;
	void Start () {
		
	}
	public Vector2Int GetGridPos()
	{

		return new Vector2Int
		(	Mathf.RoundToInt(transform.position.x/gridSize),
			Mathf.RoundToInt(transform.position.z/gridSize)
		);
	}
	public int GetGridSize()
	{
		return gridSize;
	}
	// Update is called once per frame
	public void SetMaterialColor(Color color)
	{
		MeshRenderer upMeshRenderer=transform.Find("up").GetComponent<MeshRenderer>();
		upMeshRenderer.material.color=color;
	}
	void Update()
	{
		if(isExplored)
		{
			this.SetMaterialColor(Color.blue);
		}
	}
	void OnMouseOver()
	{
		if(Input.GetMouseButtonDown(0))
		{
			if(isPlaceAble)
			{
				TowerFactory towerFactory=FindObjectOfType<TowerFactory>();
				towerFactory.AddTower(this);
			}
			else
			{
				print("Can't place here");
			}
			
		}

	}
}
