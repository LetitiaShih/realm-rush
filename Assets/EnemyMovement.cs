﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

	[SerializeField] float moveSpeed=1;
	void Start () {
		PathFinding pathFinding=FindObjectOfType<PathFinding>();
		var path=pathFinding.GetPath();
		StartCoroutine(FollowPath(path));
	}

	IEnumerator FollowPath(List<WayPoint> path)
	{
		foreach (WayPoint wayPoints in path)
		{
			transform.position=wayPoints.transform.position;
			yield return new WaitForSeconds(moveSpeed);
		}
	}	
}
