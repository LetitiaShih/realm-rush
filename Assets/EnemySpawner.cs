﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	[SerializeField] GameObject enemy;
	[SerializeField] Transform enemyParentTransform;
	[SerializeField] float secondBetweenSpawn=3f; 
	void Start () {
		StartCoroutine(SpawnEnemies());
	}
	


	IEnumerator SpawnEnemies()
	{
		while(true)
		{
			
			var newEnemy=Instantiate(enemy,new Vector3(0,0,0),Quaternion.identity);
			newEnemy.transform.parent=enemyParentTransform;
			yield return new WaitForSeconds(secondBetweenSpawn);			
		}

	}

}
