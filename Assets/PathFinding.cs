﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour {
	WayPoint searchCenter;
	[SerializeField]WayPoint startWayPoint, endWaypoint;
	Vector2Int[] directions={Vector2Int.up,Vector2Int.right,Vector2Int.down,Vector2Int.left}; 
	Dictionary<Vector2Int,WayPoint> grid=new Dictionary<Vector2Int, WayPoint>();
	Queue<WayPoint> queue=new Queue<WayPoint>(); 
	[SerializeField]List<WayPoint> path=new List<WayPoint>();
	bool isRunning=true;
	void Start () {
	}
	
	// Update is called once per frame
	void ColorStartAndEnd()
	{
		startWayPoint.SetMaterialColor(Color.yellow);
		endWaypoint.SetMaterialColor(Color.green);
	}
	void ExploreNeighbor()
	{
		if(!isRunning)
		{
			return;
		}
		foreach(Vector2Int direction in directions)
		{
			Vector2Int neighborCoordinates=searchCenter.GetGridPos()+direction;
			if(grid.ContainsKey(neighborCoordinates))
			{
				QueueNewNeighbor(neighborCoordinates);
			}

		}

	}
	void QueueNewNeighbor(Vector2Int neighborCoordinates)
	{		
		WayPoint neighbor=grid[neighborCoordinates];
		if(neighbor.isExplored||queue.Contains(neighbor))
		{
			//do nothing
		}
		else
		{
			queue.Enqueue(neighbor);
			neighbor.exploredFrom=searchCenter;
			//print("Queueing "+neighbor);
		}

	}
	void CreadPath()
	{
		SetAsPath(endWaypoint);
		WayPoint previous=endWaypoint.exploredFrom;
		while(previous!=startWayPoint)
		{
			SetAsPath(previous);
			previous=previous.exploredFrom;
		}
		SetAsPath(startWayPoint);
		path.Reverse();
	}
	void SetAsPath(WayPoint wayPoint)
	{
			path.Add(wayPoint);
			wayPoint.isPlaceAble=false;		
	}
	public List<WayPoint> GetPath()
	{
		if(path.Count==0)
		{
			CalculatePath();
		}

		return path;
	}
	void CalculatePath()
	{
		LoadBlocks();
		ColorStartAndEnd();
		BreadthFirstSearch();
		CreadPath();
	}
	void BreadthFirstSearch()
	{
		queue.Enqueue(startWayPoint);
		while(queue.Count>0&&isRunning)
		{
			searchCenter=queue.Dequeue();
			//print("Searching from "+searchCenter);
			HaltIfEndFound();
			ExploreNeighbor();
			searchCenter.isExplored=true;
		}
		//print("finish pathfinding?");
	}
	void HaltIfEndFound()
	{
		if(searchCenter==endWaypoint)
		{
			print("Search from End point, stop.");
			isRunning=false;
		}
	}
	void LoadBlocks()
	{
		var wayPoints=FindObjectsOfType<WayPoint>();
		foreach(WayPoint waypoint in wayPoints)
		{
			var gridPos=waypoint.GetGridPos();
			if(grid.ContainsKey(gridPos))
			{
				Debug.LogWarning("Skipping overlap block "+waypoint);
			}
			else
			{
				grid.Add(gridPos,waypoint);
			}
		}
		print("Load "+grid.Count+" blocks");
	}
}
