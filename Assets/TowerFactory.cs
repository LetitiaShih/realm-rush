﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour {
	[SerializeField] Tower tower;
	Queue <Tower> TowersQueue=new Queue <Tower>();
	[SerializeField]int towerLimit=5;
	[SerializeField] Transform towerParentTransform;

	public void AddTower(WayPoint baseWayPoint)
	{
		int numTowers=TowersQueue.Count;
		if(numTowers<towerLimit)
		{
			InstantiateNewTower(baseWayPoint);
		}
		else
		{
			RemoveTower(baseWayPoint);
		}
	}
	void InstantiateNewTower(WayPoint baseWayPoint)
	{


			Vector3 towerPos=new Vector3(baseWayPoint.transform.position.x,5,baseWayPoint.transform.position.z);
			Tower newTower=Instantiate(tower,towerPos,Quaternion.identity);
			newTower.transform.parent=towerParentTransform;
			baseWayPoint.isPlaceAble=false;

			newTower.baseWayPoint=baseWayPoint;
			TowersQueue.Enqueue(newTower);

	}
	void RemoveTower(WayPoint newBaseWayPoint)
	{
		Tower oldTower=TowersQueue.Dequeue();

		oldTower.baseWayPoint.isPlaceAble=true;
		newBaseWayPoint.isPlaceAble=false;

		oldTower.baseWayPoint=newBaseWayPoint;
		oldTower.transform.position=new Vector3(newBaseWayPoint.transform.position.x,5,newBaseWayPoint.transform.position.z);

		TowersQueue.Enqueue(oldTower);
	}

}
