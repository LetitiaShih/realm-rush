﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

	[SerializeField] Transform objectOnPan;
	[SerializeField] Transform targetEnemy;
	[SerializeField] ParticleSystem projectileParticle
	;
	[SerializeField] float attackDistance=40f;
	public WayPoint baseWayPoint;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		SetTargetEnemy();
		if(targetEnemy)
		{
			objectOnPan.LookAt(targetEnemy);
			FireEnemy();
		}
		else
		{
			Shoot(false);
		}

	}
	void SetTargetEnemy()
	{
		var sceneEnemies=FindObjectsOfType<EnemyMovement>();
		if(sceneEnemies.Length==0)
		{
			return;
		}
		Transform closetEnemy=sceneEnemies[0].transform;
		foreach(EnemyMovement testEnemy in sceneEnemies)
		{
			closetEnemy=GetClosetEnemy(closetEnemy,testEnemy.transform);

		}
		targetEnemy=closetEnemy;

	}
	Transform GetClosetEnemy(Transform transFormA,Transform transformB)
	{
		var distToA=Vector3.Distance(gameObject.transform.position,transFormA.position);
		var distToB=Vector3.Distance(gameObject.transform.position,transformB.transform.position);
		if(distToB<distToA)
		{
			return transformB;
		}
		else
		{
			return transFormA;
		}
	}
	void FireEnemy()
	{
		float distanceBetween =Vector3.Distance(targetEnemy.transform.position,gameObject.transform.position);
		//print("Distance Between: "+distanceBetween);
		if(distanceBetween<=attackDistance)
		{
			Shoot(true);
		}
		else
		{
			Shoot(false);
		}
	}
	void Shoot(bool isActive)
	{
		var emissionModule=projectileParticle.emission;
		emissionModule.enabled=isActive;
	}
}
