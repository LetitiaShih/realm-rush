﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {
	[SerializeField] int MaxHp=10;
	[SerializeField]int Damage=1;
	[SerializeField] ParticleSystem deathEffect;
	[SerializeField] int Hp;

	// Use this for initialization
	void Start () {
		Hp=MaxHp;
	}
	
	// Update is called once per frame
	private void OnTriggerEnter(Collider other)
	{
		if(Hp>1)
		{
			Hp=Hp-Damage;
		}
		else
		{
		var deathEffectSpawnPos=new Vector3(transform.position.x,5, transform.position.z);
		var deathEffectIns=Instantiate(deathEffect,deathEffectSpawnPos,Quaternion.identity);
		deathEffectIns.Play();
		Destroy(deathEffectIns.gameObject,deathEffectIns.main.duration);
		Destroy(gameObject);	
		}
		
	}
}
