﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour {

	[SerializeField] Collider colliderMesh;
	[SerializeField] ParticleSystem hitEffect;
	[SerializeField] ParticleSystem deathEffect;
	[SerializeField] int damageEnemy=1;

	int enemyLife=10;
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnParticleCollision(GameObject other)
	{
		ProcessHit();
		if(enemyLife<=0)
		{
			KillEnemy();
		}
	}
	void ProcessHit()
	{
		enemyLife-=damageEnemy;
		hitEffect.Play();
		//print("current life"+enemyLife);
	}
	void KillEnemy()
	{
		var deathEffectSpawnPos=new Vector3(transform.position.x,7, transform.position.z);
		var deathEffectIns=Instantiate(deathEffect,deathEffectSpawnPos,Quaternion.identity);
		deathEffectIns.Play();
		Destroy(deathEffectIns.gameObject,deathEffectIns.main.duration);
		Destroy(gameObject);
	}
}
